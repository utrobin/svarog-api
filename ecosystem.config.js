module.exports = {
  apps : [{
    name: 'svarog-api',
    script: 'dist/main.js',
    instances: "max",
    exec_mode: "cluster",
  }],
};
