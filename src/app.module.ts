import {
  Inject,
  Logger,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import * as RedisStore from 'connect-redis';
import * as session from 'express-session';
import * as passport from 'passport';
import * as Sentry from '@sentry/node';
import { RedisClientType } from 'redis';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { RedisModule } from './redis/redis.module';
import { REDIS } from './redis/redis.constants';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MarathonsModule } from './marathons/marathons.module';
import { LessonsModule } from './lessons/lessons.module';
import { SubscriptionsModule } from './subscriptions/subscriptions.module';
import * as fs from 'fs';

@Module({
  imports: [
    AuthModule,
    RedisModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'rc1b-5jfxvq2ees1iqkp4.mdb.yandexcloud.net',
      port: 6432,
      username: 'utrobin',
      password: '54154167q',
      database: 'svarog',
      autoLoadEntities: true,
      synchronize: true,
      ssl: {
        rejectUnauthorized: false,
      },
    }),
    MarathonsModule,
    LessonsModule,
    SubscriptionsModule,
  ],
  providers: [AppService, Logger],
  controllers: [AppController],
})
export class AppModule implements NestModule {
  constructor(@Inject(REDIS) private readonly redis: RedisClientType) {}
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        session({
          store: new (RedisStore(session))({
            client: this.redis,
            logErrors: true,
          }),
          saveUninitialized: false,
          secret: 'sup3rs3cr3t',
          resave: false,
          cookie: {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 30 * 3,
          },
        }),
        passport.initialize(),
        passport.session(),
      )
      .forRoutes('*');

    consumer.apply(Sentry.Handlers.requestHandler()).forRoutes({
      path: '*',
      method: RequestMethod.ALL,
    });
  }
}
