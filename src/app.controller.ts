import { Controller, UseGuards, Get, Req } from '@nestjs/common';
import { Request } from 'express';
import { AppService } from './app.service';
import { AdminGuard } from 'common/guards/admin.guard';
import { LoggedInGuard } from 'common/guards/logged-in.guard';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getHello(): Promise<string> {
    return this.appService.getHello();
  }

  @UseGuards(AdminGuard)
  @Get('admin')
  getAdminMessage() {
    return 'Private admin';
  }
}
