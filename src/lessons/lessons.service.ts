import {
  BadRequestException,
  ForbiddenException,
  forwardRef,
  GoneException,
  Inject,
  Injectable,
} from '@nestjs/common';
import { CreateLessonDto } from './dto/create-lesson.dto';
import { UpdateLessonDto } from './dto/update-lesson.dto';
import { Lesson } from 'lessons/lesson.entity';
import { MarathonsService } from 'marathons/marathons.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Marathon } from 'marathons/marathon.entity';
import { UserShort } from 'auth/serialization.provider';

@Injectable()
export class LessonsService {
  constructor(
    private marathonsService: MarathonsService,
    @InjectRepository(Lesson)
    private readonly lessonsRepository: Repository<Lesson>,
  ) {}

  fillLesson(lesson: Lesson, data: UpdateLessonDto | CreateLessonDto) {
    const { name, description, videoLink } = data;

    if (name) {
      lesson.name = name;
    }

    if (description) {
      lesson.description = description;
    }

    if (videoLink) {
      lesson.videoLink = videoLink;
    }
  }

  async create(createLessonDto: CreateLessonDto) {
    const marathon = await this.marathonsService.findOne(
      createLessonDto.marathonId,
    );

    const lesson = new Lesson();
    this.fillLesson(lesson, createLessonDto);
    lesson.marathon = marathon;

    return (await this.lessonsRepository.save(lesson)).id;
  }

  async update(id: string, updateLessonDto: UpdateLessonDto) {
    if (!(await this.allowEdit(id))) {
      throw new ForbiddenException();
    }

    const lesson = await this.lessonsRepository.findOne({ where: { id } });

    if (!lesson) {
      throw new GoneException();
    }

    this.fillLesson(lesson, updateLessonDto);

    return (await this.lessonsRepository.save(lesson)).id;
  }

  findAll() {
    return `This action returns all lessons`;
  }

  findOne(id: number) {
    return `This action returns a #${id} lesson`;
  }

  remove(id: number) {
    return `This action removes a #${id} lesson`;
  }

  async allowEdit(id: string, user?: UserShort) {
    const lesson = await this.lessonsRepository.findOne({
      where: { id },
      relations: ['marathon'],
    });

    return lesson?.marathon.author === user?.id || user?.role === 'admin';
  }
}
