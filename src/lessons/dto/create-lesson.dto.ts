import { IsDefined } from 'class-validator';

export class CreateLessonDto {
  @IsDefined({ message: 'required' })
  marathonId: string;
  @IsDefined({ message: 'required' })
  name: string;
  @IsDefined({ message: 'required' })
  description: string;
  @IsDefined({ message: 'required' })
  videoLink: string;
}
