import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Marathon } from 'marathons/marathon.entity';

@Entity()
export class Lesson {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column('varchar', { length: 80 }) name: string;
  @Column('text') description: string;

  @Column() videoLink: string;

  @ManyToOne(() => Marathon, (marathon) => marathon.lessons)
  marathon: Marathon;
}
