import { Controller, Post, UseGuards, Req, Get } from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from './auth.service';
import { LocalGuard } from 'common/guards/local.guard';
import { LoggedInGuard } from 'common/guards/logged-in.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalGuard)
  @Post('login')
  async login() {
    return {};
  }

  @Get('logout')
  async logout(@Req() req: Request) {
    req.logOut();
    return {};
  }

  @UseGuards(LoggedInGuard)
  @Get('user/short')
  userShort(@Req() req: Request) {
    return req.user;
  }

  @UseGuards(LoggedInGuard)
  @Get('user')
  user(@Req() req: Request) {
    return this.authService.user((req as any).user.id);
  }
}
