import { Injectable, UnauthorizedException } from '@nestjs/common';
import { compare } from 'bcrypt';
import { UsersService } from 'users/users.service';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);

    if (!user || !(await compare(pass, user.password))) {
      throw new UnauthorizedException('Incorrect username or password');
    }

    const { password: _, ...retUser } = user;
    return retUser;
  }

  async user(id: string) {
    const user = await this.usersService.findOne(id);

    if (!user) {
      throw new UnauthorizedException();
    }

    const { password: _, ...retUser } = user;
    return retUser;
  }
}
