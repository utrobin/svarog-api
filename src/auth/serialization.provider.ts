import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';
import { UsersService } from 'users/users.service';
import { User } from 'users/user.entity';

export type UserShort = Pick<User, 'id' | 'role' | 'email'>;

@Injectable()
export class AuthSerializer extends PassportSerializer {
  constructor() {
    super();
  }

  serializeUser(user: any, done: (err: Error | null, user: UserShort) => void) {
    done(null, { id: user.id, role: user.role, email: user.email });
  }

  deserializeUser(
    payload: UserShort,
    done: (err: Error | null, user: UserShort) => void,
  ) {
    done(null, payload);
  }
}
