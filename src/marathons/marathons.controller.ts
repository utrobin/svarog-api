import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Request,
  UseGuards,
  Query,
} from '@nestjs/common';
import { MarathonsService } from './marathons.service';
import { CreateMarathonDto } from './dto/create-marathon.dto';
import { UpdateMarathonDto } from './dto/update-marathon.dto';
import { LoggedInGuard } from 'common/guards/logged-in.guard';

@Controller('marathons')
export class MarathonsController {
  constructor(private readonly marathonsService: MarathonsService) {}

  @UseGuards(LoggedInGuard)
  @Post()
  create(@Request() req: Req, @Body() createMarathonDto: CreateMarathonDto) {
    return this.marathonsService.create(req.user, createMarathonDto);
  }

  @Get()
  findAll(@Request() req: Req, @Query() query: { my?: string }) {
    if (query.my) {
      return this.marathonsService.findAllMy(req.user);
    }

    return this.marathonsService.findAll();
  }

  @Get(':id')
  async findOne(@Request() req: Req, @Param('id') id: string) {
    const marathon = await this.marathonsService.findOne(id);
    const allowEdit = await this.marathonsService.allowEdit(marathon, req.user);

    return {
      ...marathon,
      ...(allowEdit ? { allowEdit } : {}),
    };
  }

  @UseGuards(LoggedInGuard)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMarathonDto: UpdateMarathonDto,
    @Request() req: Req,
  ) {
    return this.marathonsService.update(id, req.user, updateMarathonDto);
  }

  @UseGuards(LoggedInGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.marathonsService.remove(id);
  }
}
