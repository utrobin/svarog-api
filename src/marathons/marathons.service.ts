import {
  BadRequestException,
  ForbiddenException,
  GoneException,
  Injectable,
} from '@nestjs/common';
import { CreateMarathonDto } from './dto/create-marathon.dto';
import { UsersService } from 'users/users.service';
import { Marathon } from 'marathons/marathon.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { first } from 'lodash';
import { UserShort } from 'auth/serialization.provider';
import { UpdateMarathonDto } from 'marathons/dto/update-marathon.dto';
import { User } from 'users/user.entity';
import { Lesson } from 'lessons/lesson.entity';

@Injectable()
export class MarathonsService {
  constructor(
    private usersService: UsersService,
    @InjectRepository(Marathon)
    private readonly marathonsRepository: Repository<Marathon>,
  ) {}

  fillMarathon(
    marathon: Marathon,
    data: UpdateMarathonDto | CreateMarathonDto,
    author: User,
  ) {
    const { name, description, image } = data;

    if (name) {
      marathon.name = name;
    }

    if (description) {
      marathon.description = description;
    }

    if (image) {
      marathon.image = image;
    }

    marathon.author = author;
  }

  async create(user: UserShort, createMarathonDto: CreateMarathonDto) {
    const author = await this.usersService.findOne(user.id);

    if (!author) {
      throw new GoneException();
    }

    const marathon = new Marathon();
    this.fillMarathon(marathon, createMarathonDto, author);

    return (await this.marathonsRepository.save(marathon)).id;
  }

  async update(
    id: string,
    user: UserShort,
    updateMarathonDto: UpdateMarathonDto,
  ) {
    const author = await this.usersService.findOne(user.id);

    const marathon = await this.marathonsRepository.findOne({
      where: { id },
    });

    if (!author || !marathon) {
      throw new GoneException();
    }

    if (!(await this.allowEdit(marathon, author))) {
      throw new ForbiddenException();
    }

    this.fillMarathon(marathon, updateMarathonDto, author);

    return (await this.marathonsRepository.save(marathon)).id;
  }

  findAll() {
    return this.marathonsRepository.find();
  }

  findAllMy(user: UserShort) {
    return this.marathonsRepository.findBy({ author: user });
  }

  async findOne(id: string) {
    const marathon = first(
      await this.marathonsRepository.find({
        where: { id },
        relations: ['author', 'subscribers', 'lessons'],
      }),
    );

    if (!marathon) {
      throw new BadRequestException('bad id');
    }

    return marathon;
  }

  remove(id: string) {
    return this.marathonsRepository.delete({ id });
  }

  async addLesson(marathon: Marathon, lesson: Lesson) {
    if (!marathon) {
      throw new BadRequestException('bad id');
    }

    marathon.lessons = [lesson];
    await this.marathonsRepository.save(marathon);
  }

  async allowEdit(marathon: Marathon, user?: UserShort) {
    return marathon.author.id === user?.id || user?.role === 'admin';
  }
}
