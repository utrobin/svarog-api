import { IsDefined } from 'class-validator';

export class CreateMarathonDto {
  @IsDefined({ message: 'required' })
  name: string;

  @IsDefined({ message: 'required' })
  description: string;

  @IsDefined({ message: 'required' })
  image: string;
}
