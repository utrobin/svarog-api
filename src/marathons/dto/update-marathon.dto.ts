import { PartialType } from '@nestjs/mapped-types';
import { CreateMarathonDto } from './create-marathon.dto';

export class UpdateMarathonDto extends PartialType(CreateMarathonDto) {}
