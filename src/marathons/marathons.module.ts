import { Module } from '@nestjs/common';
import { MarathonsService } from './marathons.service';
import { MarathonsController } from './marathons.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Marathon } from 'marathons/marathon.entity';
import { UsersModule } from 'users/users.module';

@Module({
  imports: [UsersModule, TypeOrmModule.forFeature([Marathon])],
  exports: [MarathonsService],
  controllers: [MarathonsController],
  providers: [MarathonsService],
})
export class MarathonsModule {}
