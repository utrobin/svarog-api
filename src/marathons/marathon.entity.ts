import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from 'users/user.entity';
import { Lesson } from 'lessons/lesson.entity';

@Entity()
export class Marathon {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column('varchar', { length: 80 }) name: string;
  @Column('text') description: string;
  @Column() image: string;

  @ManyToOne(() => User, (user) => user.id)
  author: User;

  @ManyToMany(() => User, (user) => user.subscriptions)
  @JoinTable()
  subscribers: User[];

  @OneToMany(() => Lesson, (lesson) => lesson.marathon)
  lessons: Lesson[];
}
