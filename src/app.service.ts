import { Inject, Injectable } from '@nestjs/common';
import { REDIS } from 'redis/redis.constants';
import { RedisClientType } from 'redis';

@Injectable()
export class AppService {
  constructor(@Inject(REDIS) private readonly redis: RedisClientType) {}

  async getHello(): Promise<string> {
    await this.redis.set('foo', 'testik');
    const value = await this.redis.get('foo');
    return `Hello World! ${value}`;
  }
}
