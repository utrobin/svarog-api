import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { first } from 'lodash';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: true,
    credentials: true,
  });
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => {
        return new BadRequestException(
          errors.map(({ property, constraints }) => [
            property,
            constraints && first(Object.values(constraints)),
          ]),
        );
      },
    }),
  );
  await app.listen(3000);
}

bootstrap();
