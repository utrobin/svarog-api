import { BadRequestException, Injectable } from '@nestjs/common';
import { UserShort } from 'auth/serialization.provider';
import { UsersService } from 'users/users.service';
import { MarathonsService } from 'marathons/marathons.service';

@Injectable()
export class SubscriptionsService {
  constructor(
    private marathonsService: MarathonsService,
    private usersService: UsersService,
  ) {}

  async create(marathonId: string, userShort: UserShort) {
    const [marathon, user] = await Promise.all([
      this.marathonsService.findOne(marathonId),
      this.usersService.findOne(userShort.id),
    ]);

    if (!marathon || !user) {
      throw new BadRequestException('bad id');
    }

    await this.usersService.addSubscription(user, marathon);

    return {};
  }

  async remove(marathonId: string, userShort: UserShort) {
    const user = await this.usersService.findOne(userShort.id);

    if (!user) {
      throw new BadRequestException('bad userId');
    }

    await this.usersService.deleteSubscription(user, marathonId);

    return {};
  }

  async findOne(marathonId: string, userShort: UserShort) {
    const user = await this.usersService.findOne(userShort.id);

    if (!user) {
      throw new BadRequestException('bad userId');
    }

    console.log(user, marathonId, 999);
    return {
      subscribed: user.subscriptions.some(({ id }) => id === marathonId),
    };
  }
}
