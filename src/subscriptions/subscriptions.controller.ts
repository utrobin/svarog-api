import {
  Controller,
  Get,
  Post,
  Param,
  Delete,
  Request,
  UseGuards,
  Query,
} from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { LoggedInGuard } from 'common/guards/logged-in.guard';

@Controller('subscriptions')
export class SubscriptionsController {
  constructor(private readonly subscriptionsService: SubscriptionsService) {}

  @UseGuards(LoggedInGuard)
  @Post()
  create(@Request() req: Req, @Query() query: any) {
    return this.subscriptionsService.create(query.marathonId, req.user);
  }

  @UseGuards(LoggedInGuard)
  @Delete()
  remove(@Request() req: Req, @Query() query: any) {
    return this.subscriptionsService.remove(query.marathonId, req.user);
  }

  @UseGuards(LoggedInGuard)
  @Get()
  findOne(@Request() req: Req, @Query() query: any) {
    return this.subscriptionsService.findOne(query.marathonId, req.user);
  }
}
