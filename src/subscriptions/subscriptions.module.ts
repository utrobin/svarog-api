import { Module } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionsController } from './subscriptions.controller';
import { MarathonsModule } from 'marathons/marathons.module';
import { UsersModule } from 'users/users.module';

@Module({
  imports: [MarathonsModule, UsersModule],
  controllers: [SubscriptionsController],
  providers: [SubscriptionsService],
})
export class SubscriptionsModule {}
