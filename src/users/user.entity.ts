import {
  BeforeInsert,
  Column,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import {
  IsEmail,
  IsDate,
  MinLength,
  MaxLength,
  IsOptional,
} from 'class-validator';
import { hash } from 'bcrypt';
import { Marathon } from 'marathons/marathon.entity';

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
  OTHER = 'other',
}

export enum UserRole {
  ADMIN = 'admin',
  USER = 'user',
}

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column('varchar', { length: 50, unique: true })
  @IsEmail({}, { message: 'invalid' })
  email: string;

  @Column('varchar', { length: 50, nullable: true }) firstName: string;

  @Column('varchar', { length: 80, nullable: true }) lastName: string;

  @Column('varchar', { length: 60 })
  @MinLength(8, { message: 'short' })
  @MaxLength(60, { message: 'long' })
  password: string;

  @Column({
    type: 'enum',
    enum: Gender,
    default: Gender.OTHER,
  })
  gender: Gender;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.USER,
  })
  role: UserRole;

  @Column({ default: true }) active: boolean;

  @Column({
    type: 'date',
    nullable: true,
  })
  @IsDate()
  @IsOptional()
  birthday: Date | null;

  @BeforeInsert()
  async hashPassword() {
    this.password = await hash(this.password, 2);
  }

  @ManyToMany(() => Marathon, (marathon) => marathon.subscribers)
  subscriptions: Marathon[];
}
