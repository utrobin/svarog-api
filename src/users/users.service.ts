import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { Marathon } from 'marathons/marathon.entity';
import { UserShort } from 'auth/serialization.provider';
// import { CreateUserDto } from './dto/create-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';

export interface UserL {
  uuid: string;
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  role: string;
}

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: User): Promise<User> {
    const user = new User();
    const { email, password } = createUserDto;

    const existingUser = await this.findOneByEmail(email);
    if (existingUser) {
      throw new BadRequestException([['email', 'exist']]);
    }

    user.email = email;
    user.password = password;

    return this.usersRepository.save(user);
  }

  findAll() {
    return `This action returns all users`;
  }

  async findOne(id: string): Promise<User | null> {
    return this.usersRepository.findOne({
      where: { id },
      relations: ['subscriptions'],
    });
  }

  async findOneByEmail(email: string): Promise<User | null> {
    return this.usersRepository.findOneBy({ email });
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }

  async addSubscription(user: User, marathon: Marathon) {
    user.subscriptions.push(marathon);

    await this.usersRepository.save(user);
  }

  async deleteSubscription(user: User, marathonId: string) {
    user.subscriptions = user.subscriptions.filter(
      (marathon) => marathon.id !== marathonId,
    );

    await this.usersRepository.save(user);
  }
}
