import { Module } from '@nestjs/common';
import Redis from 'ioredis';

import { REDIS } from './redis.constants';

@Module({
  providers: [
    {
      provide: REDIS,
      useValue: new Redis({
        port: 6379, // Redis port
        host: 'c-c9q3ocfovsag3lcsfarc.rw.mdb.yandexcloud.net', // Redis host
        name: 'svarog', // needs Redis >= 6
        password: '54154167q',
        role: 'master',
      }),
    },
  ],
  exports: [REDIS],
})
export class RedisModule {}
