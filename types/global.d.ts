import { Request as RequestExpress } from 'express';
import { UserShort } from 'auth/serialization.provider';

declare global {
  interface Req extends RequestExpress {
    user: UserShort;
  }
}
